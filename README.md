# About

This is a command-line tool to generate MAC authentication rules for our HelpSpot requests.

# License

Licensed under the GNU General Public License 2.0. Mostly this means that you are free to distribute this program to anyone you choose, though you must also provide them with the source code, including the code for any modifications you have made.

# Building

If you are not planning on doing development with this project, then please skip this section and proceed to "Installing".

1.  Install Rust at <https://www.rust-lang.org/tools/install>.
2.  Within the project directory, run the following command:

    ```sh
    cargo build --release
    ```

    This should take around 3 minutes, depending on your hardware. For test builds, leave out `--release`.

# Installing and Running

From the right sidebar of this GitLab repository, click on "Tags" (just beneath the project title) and navigate to the latest release. Download the zip file for your operating system. Once the download is complete, extract it. You may move the executable to any location you choose. Either take note of this location and `cd` to it as needed, or add it to a directory in your `PATH` environment variable.

## Windows

Open a console or terminal (PowerShell is suggested on Windows, but see note below). Change your directory to the location of the executable:

```powershell
cd C:\path\to\executable\directory
```

From there it should be possible to run the executable with `.\rulemaker.exe`.

If you are using PowerShell, then it's suggested to pipe the output into your system clipboard to preserve tab characters:

```powershell
.\rulemaker.exe | set-clipboard
```

You can now paste the new rule into your spreadsheet as normal.

*Note:* By default, PowerShell removes tab characters that are pasted into it. For this program to work, you will have to right-click your window bar, go to preferences, and uncheck "Filter clipboard content on paste". The program will remind you of this when run.

