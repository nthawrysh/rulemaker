const TEST_DATA: &str =
"Name *\t\tFirstname Lastname
Campus Username *\t\t
student000
Room Number *\t\t
c000
Bedroom Number *\t\t
0
Housing *\t\t
UVA
Phone Number *\t\t(000) 000-0000
Campus Email *\t\tstudent000@cougars.csusm.edu
Console *\t\t
Nintendo DS
Connection *\t\t
Wireless
MAC Address *\t\t
00:00:00:00:00:00";

#[test]
fn get_pairs() {
    const FIELDS: [(&str, &str); 10] = [
        ("Name",                     "Firstname Lastname"),
        ("Campus Username",          "student000"),
        ("Room Number", "c000"),
        ("Bedroom Number",           "0"),
        ("Housing",                  "UVA"),
        ("Phone Number",             "(000) 000-0000"),
        ("Campus Email",             "student000@cougars.csusm.edu"),
        ("Console",                  "Nintendo DS"),
        ("Connection",               "Wireless"),
        ("MAC Address",              "00:00:00:00:00:00"),
    ];

    let pairs = super::get_pairs(TEST_DATA);

    println!("{:?}", pairs);

    for (key, val) in &pairs { // Print some diagnostic info
        println!("{}: {}", &key, val);
    }

    for (key, val) in FIELDS {
        assert_eq!(
            pairs.get(key),
            Some(&String::from(val))
        );
    }
}
