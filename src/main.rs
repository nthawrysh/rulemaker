extern crate chrono;
extern crate utils;
extern crate manuf;

use chrono::{TimeZone,Datelike};
use utils::{MacAddress, MacPattern, Vendor};
use std::io::Write; // `Stderr::flush`

pub mod device;
#[cfg(test)]
mod test;

use crate::device::*;
use std::io::Read;
use std::collections::BTreeMap;

#[cfg(target_family="unix")]
const EOF_KEY: &str = "Ctrl-D";
#[cfg(target_family="windows")]
const EOF_KEY: &str = "Ctrl-Z";

const DATE_FORMAT: &str = "%-m/%-d/%Y";

pub fn read() -> String {
    let mut text = String::new();
    std::io::stdin().read_to_string(&mut text);
    text
}

pub fn yes() -> bool {
    eprint!(" (y/n) ");
    std::io::stderr().flush();

    let mut buf = String::new();
    std::io::stdin().read_line(&mut buf);

    #[cfg(target_family="unix")]
    { buf == "y\n" }

    #[cfg(target_family="windows")]
    { buf == "y\r\n" }
}

pub fn get_pairs(text: &str) -> BTreeMap<String, String> {
    let mut text = text .replace("\t\t", "\n");
    let mut items = text.lines()
        .filter(|ln| !ln.is_empty());

    let mut pairs = BTreeMap::new();

    while let (Some(key), Some(val)) = (items.next(), items.next()) {
        assert!(!key.is_empty(), "Empty key for value {}", val);
        assert!(!val.is_empty(), "Empty value for key {}", key);

        let mut key = String::from(key);
        if key.ends_with(" *") {
            key.pop();
            key.pop();
        }

        pairs.insert(key, String::from(val));
    }

    pairs
}

pub fn lookup_vendor(mac_address: &MacAddress) -> Option<&'static Vendor<'static>> {
    let dummy = Vendor { // Unappealing hack
        mac: MacPattern::Full(*mac_address),
        id: "",
        description: "",
    };
    if let Some(ven) = manuf::KNOWN_VENDORS.get_key(&dummy) {
        return Some(ven);
    }

    // Try again with the prefix only.
    let dummy = Vendor {
        mac: MacPattern::Prefix(mac_address.prefix()),
        id: "",
        description: "",
    };
    manuf::KNOWN_VENDORS.get_key(&dummy)
}

fn main() {
    eprintln!("Now reading from stdin. Paste your customer information here, then press Enter followed by {EOF_KEY}.");
    #[cfg(target_family="windows")]
    eprintln!("Tip: If pasting doesn't work properly on PowerShell, right click the top window bar, go to Properties, and uncheck \"Filter clipboard content on paste\".");

    let text = read();
    eprintln!();
    let pairs = get_pairs(&text);

    // TODO: Better (perhaps regex-based) validation would be ideal in general.

    let full_name = pairs.get("Name")
        .expect("Missing field: Name");

    let (first_name, last_name) = {
        let mut iter = full_name.split(" ");
        (
            iter.next().unwrap(),
            iter.next().expect("No last name given"),
        )
    };

    let username = match (pairs.get("Campus Username"), pairs.get("Campus Email")) {
        (Some(user), email) => {
            let mut split_at_sign = user.splitn(2, '@');
            let user = split_at_sign
                .next().unwrap()
                .to_lowercase();

            // If the email was given, then verify that it matches the username.
            // We don't otherwise use the email address.
            if let Some(email) = email {
                let email = email.to_lowercase();
                assert!(
                    email.starts_with(&user),
                    "Username and email address do not match"
                );
                if let Some(domain) = split_at_sign.next() {
                    assert!(
                        email.ends_with(&domain.to_lowercase()),
                        "Username and email address do not match"
                    );
                }
            }

            user
        },

        (None, Some(email)) => email.splitn(2, "@").next()
            .expect("Invalid email address")
            .into(),

        (None, None) => panic!("Fields 'Campus Username' and 'Campus Email' both not given")
    };

    let device = match pairs.get("Console") {
        Some(console) => {
            match device::match_device(console) {
                Some(dev) => {
                    eprintln!("Inferred device type {}.", dev);
                    dev
                },
                None => {
                    eprintln!("Could not find a device type for '{}'. Please select a device from the list below.", console);
                    device::select_device()
                }
            }
        },
        None => {
            eprintln!("\nNo field 'Console' was given. Please select a device from the list below.");
            device::select_device()
        }
    };

    let mac_address: MacAddress = pairs.get("MAC Address")
        .expect("Missing field: MAC Address")
        .parse().unwrap();

    match lookup_vendor(&mac_address) {
        Some(vendor) => {
            let recognized = device.vendor_ids.iter()
                .find(|id| **id == vendor.id)
                .is_some();
            if recognized {
                eprintln!("Vendor ID \"{}\" is known for device type {}.", vendor.id, device)
            }
            else {
                eprint!(
                    "\nThe supplied MAC address matched the following vendor:\n\t{}\nIs this appropriate for device type {}?",
                    vendor, device
                );
                if !yes() { return; }
            }
        },
        None => {
            eprint!("[WARNING] Unknown vendor for MAC address {}. Proceed anyway?", mac_address);
            if !yes() { return; }
        }
    }

    let housing = {
        let housing = pairs.get("Housing")
            .or_else(|| pairs.get("Select a Choice"))
            .expect("Missing field: Housing | Select a Choice")
            .to_uppercase();

        if housing.contains("UVA") { "UVA" }
        else if housing.contains("QUAD") { "Quad" }
        else { panic!("Unknown housing: {}", housing) }
    };

    let room = {
        let room =
            if let Some(room) = pairs.get("Building and Room Number") { room }
            else {
                pairs.get("Room Number")
                    .expect("Missing field: 'Building and Room Number' or 'Room Number'")
            };
        room.replace(" ", "")
    };

    let bedroom = match pairs.get("Bedroom Number") {
        Some(bed) => String::from("-") + bed,
        None => "".into()
    };

    let (today, next_year) = {
        let today = chrono::Local::today();
        let next_year = chrono::Utc.ymd(today.year() + 1, today.month(), today.day());
        (
            today.format(DATE_FORMAT).to_string(),
            next_year.format(DATE_FORMAT).to_string()
        )
    };

    let phone = pairs.get("Phone Number")
        .expect("Missing field: Phone Number");

    eprintln!("Rule created successfully.\n");
    std::io::stderr().flush();
    println!(
        "\t{full_name}\t{housing}-{room}{bedroom}\t{phone}\t{today}\t\t{}\tlocal-userdb add username {mac_address} password {mac_address} role role-macauth-housing email {username}-{}@cougars.csusm.edu expiry time {next_year} 00:00:00",
        device.name,
        device.user_suffix,
    )
}
