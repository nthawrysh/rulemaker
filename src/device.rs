use std::fmt;

// TODO: It would be awesome to be able to send an http
// request to https://www.wireshark.org/tools/oui-lookup.html
// and automatically validate MAC addresses.

// TODO: If we introduce JSON, parsing, then consider moving these to a JSON file?
pub const KNOWN_DEVICES: [Device; 14] = [
    Device {
        name:        "Amazon Echo/Alexa",
        user_suffix: "AmazonEcho-Alexa",
        keywords:    &["echo", "alexa"],
        vendor_ids:  &["AmazonTe"],
    },
    Device {
        name:        "Amazon Firestick",
        user_suffix: "AmazonEcho-Firestick",
        keywords:    &["firestick"],
        vendor_ids:  &["AmazonTe"],
    },
    Device {
        name:        "Apple TV/Speaker",
        user_suffix: "appletv/speaker", // Wait, that's illegal. /meme
        keywords:    &["apple tv", "apple speaker"],
        vendor_ids:  &["Apple"],
    },
    Device {
        name:        "Blue-Ray Player",
        user_suffix: "bluerayplayer",
        keywords:    &["blu-ray", "blue-ray", "blu ray", "blue ray"], // The ~~correct~~ official spelling is 'Blu-Ray'.
        vendor_ids:  &[],
    },
    Device {
        name:        "ChromeCast",
        user_suffix: "chromecast",
        keywords:    &["chromecast"],
        vendor_ids:  &["Google"],
    },
    Device {
        name:        "Google Home",
        user_suffix: "GoogleHome",
        keywords:    &["google home"],
        vendor_ids:  &["Google"],
    },
    Device {
        name:        "Nintendo (Wi-Fi)",
        user_suffix: "nintendoswitch",
        keywords:    &["nintendo", "switch"],
        vendor_ids:  &["Nintendo"],
    },
    Device {
        name:        "Playstation (Wi-Fi)",
        user_suffix: "playstation",
        keywords:    &["sony", "playstation", "ps"],
        vendor_ids:  &[
            "Sony", "SonyInte",
            "HonHaiPr", // Foxconn: major manufacturer of game consoles and Sony devices
        ],
    },
    Device {
        name:        "Printer",
        user_suffix: "printer",
        keywords:    &["printer"],
        vendor_ids:  &[],
    },
    Device {
        name:        "Roku",
        user_suffix: "roku",
        keywords:    &["roku"],
        vendor_ids:  &["Roku"],
    },
    Device {
        // TODO: It's difficult to come up with a reasonably complete list of
        // keywords or vendors for this device type; additions are welcome.
        name:        "Smart TV",
        user_suffix: "smarttv",
        keywords:    &["smart tv"],
        vendor_ids:  &["SamsungE"],
    },
    Device {
        name:        "Speaker",
        user_suffix: "speaker",
        keywords:    &["speaker"],
        vendor_ids:  &[],
    },
    Device {
        name:        "XBOX (Wi-Fi)",
        user_suffix: "xbox",
        keywords:    &["xbox", "one x", "one s", "360"],
        vendor_ids:  &["Microsof"], // Clipped by character limit
    },
    Device {
        name:        "VR Headset",
        user_suffix: "VR-headset",
        keywords:    &["vr", "headset", "oculus", "quest", "vive", "valve"], // Best effort off the top of my head; feel free to expand.
        vendor_ids:  &[], // TODO
    },
];

pub struct Device {
    pub name:        &'static str,
    pub user_suffix: &'static str,
    pub keywords:    &'static [&'static str], // I would say there's a high static-to-signal ratio here.
    pub vendor_ids:  &'static [&'static str], // *rimshot*
}

impl Device {
    pub fn keyword_match<'a>(&'a self, field: &str) -> Option<&'a str> {
        self.keywords.into_iter()
            .find(|kw| field.contains(*kw))
            .map(|found| *found)
    }
}

impl std::fmt::Display for Device {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str(self.name)
    }
}

pub fn match_device(field: &str) -> Option<&'static Device> {
    let field = field.to_lowercase();
    KNOWN_DEVICES.iter()
        .find(|dev| dev.keyword_match(&field).is_some())
}

pub fn count_digits(n: usize) -> usize {
    (n as f64).log10().floor() as usize
}

pub fn select_device() -> &'static Device {
    let choices: Vec<_> = KNOWN_DEVICES.into_iter()
        .enumerate()
        .map(|(i, dev)| (i + 1, dev.name))
        .collect();

    let num_choices = choices.len();
    let max_digits = count_digits(num_choices);

    eprintln!("");

    let mut next_pow_10 = 10;
    let mut pad_by = max_digits;
    for (i, ch) in choices {
        if i >= next_pow_10 {
            pad_by -= 1;
            next_pow_10 *= 10;
        }

        // Empty string is necessary because we're abusing a `format` feature.
        // https://stackoverflow.com/questions/35280798/printing-a-character-a-variable-number-of-times-with-println
        eprintln!("\t({})  {: <3$}{}", i, "", ch, pad_by);
    }

    eprint!("\nEnter a number (1-{}): ", num_choices);

    let choice = loop {
        let mut input = String::new();

        std::io::stdin().read_line(&mut input);
        let parsed: Result<usize, _> = input.trim().parse();

        if let Ok(dance) = parsed {
            if dance <= num_choices {
                break dance; // Haha.
            }
        }
    };

    &KNOWN_DEVICES[choice - 1]
}
