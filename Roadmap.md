Since I, Nathan, am going to be leaving soon, this document contains the plans that I had for this tool and didn't have time to fulfill.

# Server-Side Automation

Helpspot has a web API. My hope was to use it and convert this tool into a server-side program that would convert new requests to rules and then email them to operators automatically. It might be possible to set it up so that it automatically applies the rule if an operator replies with 'approve' or a similar command.

Through the web API, it might also be possible to automatically manage requests.

If you wish to take up the cause (and don't have too many cables to make), then show this repository to Justin and ask him about these ideas; see what you can do together. That had been my plan, but I never got around to it.

# Client-Side Improvements

If you aren't feeling ambitious enough to convert this into a server-side application, there are still a few potential ways to improve it as a command-line utility:

-   Better error reporting and handling. If the information given is insufficient, this program currently just panics.
-   Automatically set the clipboard, rather than requiring the user to pipe this tool's output.
-   Find a way to create rules with formatting in Google Sheets. This will take some understanding of clipboard targets.
-   Allow users to create a configuration file for device types and their keywords?
-   Improve handling of table fields.
    -   In particular, I feel that handling of room numbers could be smarter. This would just take making it aware of more possible inputs.
