use std::str::FromStr;
use std::num;
use std::fmt::{self, Display, Formatter};
use std::hash::{Hash, Hasher};
use phf_shared::{self as phf, PhfHash};

pub type MacAddress = MacFragment<6>;
pub type MacPrefix  = MacFragment<3>;

fn parse_byte(src: &str, offset: usize) -> Result<u8, ParseMacError> {
    let byte_str = &src[offset..offset + 2];
    u8::from_str_radix(byte_str, 16)
        .map_err(|err| {
            ParseMacError {
                string: String::from(src),
                err_type: ErrorType::Byte {
                    string: byte_str.into(),
                    pos: offset,
                    err
                }
            }
        })
}


/// Used in cases such as the manuf file which can either contain a full
/// or partial MAC address.
#[derive(
    Copy, Clone, Debug,
    PartialOrd, PartialEq, Ord, Eq
)]
pub enum MacPattern {
    Prefix(MacPrefix),
    Full(MacAddress),
}

impl FromStr for MacPattern {
    type Err = ParseMacError;

    fn from_str(src: &str) -> Result<Self, Self::Err> {
        MacAddress::from_str(src)
            .map(|mac| Self::Full(mac))
            .or_else(|_|
                MacFragment::from_str(src)
                    .map(|mac| Self::Prefix(mac)))
    }
}

impl Display for MacPattern {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        match self {
            Self::Prefix(mac) => mac.fmt(fmt),
            Self::Full(mac) => mac.fmt(fmt),
        }
    }
}

impl phf::FmtConst for MacPattern {
    fn fmt_const(&self, fmt: &mut Formatter) -> fmt::Result {
        match self {
            Self::Prefix(mac) => {
                write!(fmt, "MacPattern::Prefix(")?;
                mac.fmt_const(fmt)?;
                write!(fmt, ")")
            },
            Self::Full(mac) => {
                write!(fmt, "MacPattern::Full(")?;
                mac.fmt_const(fmt)?;
                write!(fmt, ")")
            },
        }
    }
}

impl Hash for MacPattern {
    fn hash<H: Hasher>(&self, state: &mut H) {
        match self {
            Self::Prefix(mac) => mac.hash(state),
            Self::Full(mac) => mac.hash(state),
        }
    }
}

impl phf::PhfHash for MacPattern {
    fn phf_hash<H: Hasher>(&self, state: &mut H) {
        match self {
            Self::Prefix(mac) => mac.phf_hash(state), // Did I ask you to deref, Rust?
            Self::Full(mac) => mac.phf_hash(state),
        }
    }
}

impl phf::PhfBorrow<Self> for MacPattern {
    fn borrow(&self) -> &Self {
        self
    }
}


#[derive(
    Copy, Clone, Debug,
    PartialOrd, PartialEq, Ord, Eq
)]
pub struct MacFragment<const N: usize>(pub [u8; N]);

impl MacAddress {
    pub fn prefix(&self) -> MacPrefix {
        (*self).into()
    }
}

impl Into<MacPrefix> for MacAddress {
    fn into(self) -> MacPrefix {
        MacPrefix::try_from(&self.0[0..3]).expect("Can't fail")
    }
}

impl<const N: usize> From<[u8; N]> for MacFragment<N> {
    fn from(fields: [u8; N]) -> Self {
        Self(fields)
    }
}

impl<const N: usize> From<&[u8; N]> for MacFragment<N> {
    fn from(fields: &[u8; N]) -> Self {
        Self(*fields)
    }
}

impl<const N: usize> std::convert::TryFrom<&[u8]> for MacFragment<N> {
    type Error = std::array::TryFromSliceError;

    fn try_from(fields: &[u8]) -> Result<Self, Self::Error> {
        Ok(Self (fields[0..N].try_into()?))
    }
}

impl<const N: usize> FromStr for MacFragment<N> {
    type Err = ParseMacError;
    fn from_str(src: &str) -> Result<Self, Self::Err> {
        let len = src.len();

        let without_separators = 2 * N;
        let with_separators = 2 * N + (N - 1);

        if len == without_separators { // No separators
            Ok(Self(
                array_init::from_iter(
                    (0..=10).step_by(3).into_iter()
                        .map(|i| parse_byte(src, i))
                        .collect::<Result<Vec<_>, _>>()? // Hack to finish fast on the first error
                        .into_iter()
                ).expect("Can't happen: Could not collect parsed MAC address into array"), // Probably a big assumption
            ))
        }

        else if len == with_separators {
            { // Validate field separators.
                let mut char_iter = src.chars();
                for i in 0..(N - 1) {
                    char_iter.next();
                    char_iter.next();

                    let ch = char_iter.next().unwrap();

                    if let ':' | '-' = ch {}
                    else {
                        return Err(ParseMacError {
                            string: src.into(),
                            err_type: ErrorType::Format,
                        });
                    }
                }
            }

            Ok(Self(
                array_init::from_iter(
                    (0..=with_separators).step_by(3).into_iter()
                        .map(|i| parse_byte(src, i))
                        .collect::<Result<Vec<_>, _>>()? // Hack to finish fast on the first error
                        .into_iter()
                ).expect("Can't happen: Could not collect parsed MAC address into array"),
            ))
        }

        else {
            Err(ParseMacError {
                string: src.into(),
                err_type: ErrorType::Format,
            })
        }
    }
}

impl<const N: usize> Display for MacFragment<N> {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        let mut out = String::with_capacity(18); // Reserve one extra byte, as a treat.

        for byte in &self.0 {
            out.push_str(&format!("{:02x}", byte)); // Format each byte as lowercase hexadecimal, min. 2 digits.
            out.push(':');
        }
        out.pop(); // Remove trailing ':'. I hope this way is tasty enough for you.

        fmt.write_str(&out)
    }
}

impl<const N: usize> Hash for MacFragment<N> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.0.hash(state)
    }
}

impl<const N: usize> phf::FmtConst for MacFragment<N> {
    fn fmt_const(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "MacFragment(*")?;
        self.0.fmt_const(fmt)?;
        write!(fmt, ")")
    }
}

impl<const N: usize> phf::PhfHash for MacFragment<N> {
    fn phf_hash<H: Hasher>(&self, state: &mut H) {
        self.0.phf_hash(state);
    }
}

impl<const N: usize> phf::PhfBorrow<Self> for MacFragment<N> {
    fn borrow(&self) -> &Self {
        self
    }
}

#[derive(Debug)]
pub struct ParseMacError {
    pub(super) string: String,
    pub(super) err_type: ErrorType,
}

impl Display for ParseMacError {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "Could not parse \"{}\" as MAC address: {}", self.string, self.err_type)
    }
}


#[derive(Debug)]
pub(super) enum ErrorType {
    Byte {
        string: String,
        pos: usize,
        err: num::ParseIntError,
    },
    Format,
}

impl Display for ErrorType {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        match self {
            ErrorType::Format => write!(fmt, "unknown format"),
            ErrorType::Byte { string, pos, err } => write!(
                fmt, "invalid byte '{string}' at offset {pos}: {err}"
            ),
            _ => unimplemented!()
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn from_str() {
        assert_eq!(
            MacAddress::from_str("01:23:45:67:89:AB").unwrap(),
            MacAddress::from(&[0x01, 0x23, 0x45, 0x67, 0x89, 0xAB])
        );
        assert_eq!(
            MacPrefix::from_str("01:23:45").unwrap(),
            MacPrefix::from(&[0x01, 0x23, 0x45])
        );
    }
}
