extern crate phf_shared;
extern crate array_init;

pub mod mac_address;
pub mod vendor;
pub use mac_address::{MacAddress, MacPrefix, MacPattern};
pub use vendor::Vendor;
