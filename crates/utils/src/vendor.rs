use crate::MacPattern;
use std::{fmt, cmp};
use std::hash::{self, Hash};
use phf_shared::{self as phf, PhfHash, FmtConst};

#[derive(Clone, Debug)]
pub struct Vendor<'a> {
    pub mac: MacPattern,
    pub id: &'a str,
    pub description: &'a str,
}

impl<'a> cmp::PartialOrd for Vendor<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<cmp::Ordering> {
        cmp::PartialOrd::partial_cmp(&self.mac, &other.mac)
    }
}

impl<'a> cmp::Ord for Vendor<'a> {
    fn cmp(&self, other: &Self) -> cmp::Ordering {
        cmp::Ord::cmp(&self.mac, &other.mac)
    }
}

impl<'a> cmp::PartialEq for Vendor<'a> {
    fn eq(&self, other: &Self) -> bool {
        self.mac == other.mac
    }
}

impl<'a> cmp::Eq for Vendor<'a> {}

impl<'a> hash::Hash for Vendor<'a> {
    fn hash<H: hash::Hasher>(&self, state: &mut H) {
        self.mac.hash(state)
    }
}

impl<'a> phf::PhfHash for Vendor<'a> {
    fn phf_hash<H: hash::Hasher>(&self, state: &mut H) {
        self.mac.phf_hash(state)
    }
}

impl<'a> fmt::Display for Vendor<'a> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(
            fmt, "{}\t{}\t{}",
            self.mac, self.id, self.description
        )
    }
}

impl<'a> phf::FmtConst for Vendor<'a> {
    fn fmt_const(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(
            fmt, "Vendor {{\n    mac: ",
        )?;
        self.mac.fmt_const(fmt)?;

        write!(fmt, ",\n    id: ")?;
        self.id.fmt_const(fmt)?;

        write!(fmt, ",\n    description: ")?;
        self.description.fmt_const(fmt)?;

        write!(fmt, "\n}}")?;

        Ok(())
    }
}

#[derive(Clone, Debug)]
pub struct VendorOwned {
    pub mac: MacPattern,
    pub id: String,
    pub description: String,
}

impl cmp::PartialOrd for VendorOwned {
    fn partial_cmp(&self, other: &Self) -> Option<cmp::Ordering> {
        cmp::PartialOrd::partial_cmp(&self.mac, &other.mac)
    }
}

impl cmp::Ord for VendorOwned {
    fn cmp(&self, other: &Self) -> cmp::Ordering {
        cmp::Ord::cmp(&self.mac, &other.mac)
    }
}

impl cmp::PartialEq for VendorOwned {
    fn eq(&self, other: &Self) -> bool {
        self.mac == other.mac
    }
}

impl cmp::Eq for VendorOwned {}

impl hash::Hash for VendorOwned {
    fn hash<H: hash::Hasher>(&self, state: &mut H) {
        self.mac.hash(state)
    }
}

impl phf::PhfHash for VendorOwned {
    fn phf_hash<H: hash::Hasher>(&self, state: &mut H) {
        self.mac.phf_hash(state)
    }
}

impl fmt::Display for VendorOwned {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(
            fmt, "{}\t{}\t{}",
            self.mac, self.id, self.description
        )
    }
}

/// NOTE: This does not output itself, but rather the non-owned version.
///       This is just an ugly hack to get the codegen to work.
impl phf::FmtConst for VendorOwned {
    fn fmt_const(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "Vendor {{\n    mac: ")?;
        self.mac.fmt_const(fmt)?;

        write!(fmt, ",\n    id: ")?;
        self.id.fmt_const(fmt)?;

        write!(fmt, ",\n    description: ")?;
        self.description.fmt_const(fmt)?;

        write!(fmt, "\n}}")?;

        Ok(())
    }
}

impl<'a> phf::PhfBorrow<Self> for Vendor<'a> {
    fn borrow(&self) -> &Self {
        self // Plaudits.
    }
}
