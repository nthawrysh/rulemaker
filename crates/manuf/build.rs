use std::fs;
use std::io::{self, BufReader, BufWriter, BufRead, Write};
use std::path::Path;
use utils::{vendor::VendorOwned, MacPrefix};
use phf_codegen as phf;

#[allow(unused)]
const THIS_ONLY_EXISTS_TO_TRICK_RUST_INTO_BUILDING_AGAIN: &str = "So how has your day been?";

fn open_file(path: &Path, write: bool) -> io::Result<fs::File> {
    fs::OpenOptions::new()
        .create_new(!path.exists())
        .write(write)
        .open(path)
}

fn manuf() {
    let in_path  = Path::new("./src/manuf");
    let out_path = Path::new("./build/manuf.rs");

    fn should_update(in_path: &Path, out_path: &Path) -> bool {
        let in_modified = in_path.metadata().unwrap()
            .modified().unwrap();

        if !out_path.exists() { return true; }

        let out_modified = out_path.metadata().unwrap()
            .modified().unwrap();

        in_modified > out_modified
    }

    if !should_update(&in_path, &out_path) { return }

    let mut manuf_rs = BufWriter::new(open_file(out_path, true).unwrap());

    let manuf_lines = BufReader::new(
        fs::File::open(in_path).unwrap()
    )
        .lines()
        .map(|ln| ln.unwrap())
        .filter(|ln| !ln.is_empty() && !ln.starts_with('#'));

    /*
     * Build the set.
     */

    let mut set = phf::Set::new();

    for ln in manuf_lines {
        let mut fields = ln.splitn(3, '\t');
        let mac_str = fields.next().unwrap();
        let mac_len = usize::min(17, mac_str.len());

        let vendor = VendorOwned {
            mac:         mac_str[..mac_len].parse().unwrap(),
            id:          fields.next().unwrap().into(),
            description: fields.next().unwrap_or("").into(),
        };

        set.entry(vendor);
    }

    writeln!(
        &mut manuf_rs,
        "pub static KNOWN_VENDORS: phf::Set<Vendor> = {};\n",
        set.build()
    ).unwrap();
}

fn main() {
    println!("cargo:rerun-if-changed=src/manuf");

    {
        let build_dir = Path::new("./build");

        if !build_dir.exists() {
            fs::create_dir(build_dir);
        }
    }

    manuf();
}
